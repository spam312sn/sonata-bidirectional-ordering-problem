<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImmutableRegion
 *
 * @ORM\Table(name="regions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImmutableRegionRepository")
 */
class ImmutableRegion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string")
     *
     * @var string
     */
    private $name;

    public function __toString()
    {
        return $this->name ?: '';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ImmutableRegion
     */
    public function setName(string $name): ImmutableRegion
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ImmutableRegion
     */
    public function setId(int $id): ImmutableRegion
    {
        $this->id = $id;

        return $this;
    }
}

