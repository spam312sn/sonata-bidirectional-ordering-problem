<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Airline
 *
 * @ORM\Table(name="airlines")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AirlineRepository")
 */
class Airline
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\RegionAirline",
     *     mappedBy="airline",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     * @ORM\OrderBy({"regionOrderPosition"="ASC"})
     *
     * @var array|Collection<RegionAirline>|RegionAirline[]
     */
    private $regions;

    public function __toString()
    {
        return $this->name ?: '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Airline
     */
    public function setId(int $id): Airline
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Airline
     */
    public function setName(string $name): Airline
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return RegionAirline[]|array|Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * @param RegionAirline[]|array|Collection $regions
     *
     * @return Airline
     */
    public function setRegions($regions)
    {
        $this->regions = $regions;

        return $this;
    }

    public function addRegion(RegionAirline $regionAirline)
    {
        $regionAirline->setAirline($this);
        $this->regions->add($regionAirline);
    }
}

