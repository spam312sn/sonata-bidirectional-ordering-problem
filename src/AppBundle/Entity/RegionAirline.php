<?php
/**
 * RegionAirline
 *
 * Created at 2019-10-10 00:17
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class RegionAirline
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     name="region_airlines",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="region_airlines", columns={"list_id", "airline_id"})}
 * )
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class RegionAirline
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\RegionAirlinesList",
     *     inversedBy="airlines",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(name="list_id", referencedColumnName="id")
     * @ORM\Id()
     *
     * @var RegionAirlinesList
     */
    private $airlinesList;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Airline",
     *     inversedBy="regions"
     * )
     * @ORM\JoinColumn(name="airline_id", referencedColumnName="id")
     * @ORM\Id()
     *
     * @var Airline
     */
    private $airline;

    /**
     * @ORM\Column(name="region_order_position", type="integer", options={"default":0})
     * @var int
     */
    private $regionOrderPosition = 0;

    /**
     * @ORM\Column(name="airline_order_position", type="integer", options={"default":0})
     * @var int
     */
    private $airlineOrderPosition = 0;

    /**
     * @return RegionAirlinesList
     */
    public function getAirlinesList()
    {
        return $this->airlinesList;
    }

    /**
     * @param RegionAirlinesList $airlinesList
     */
    public function setAirlinesList(RegionAirlinesList $airlinesList): void
    {
        $this->airlinesList = $airlinesList;
    }

    /**
     * @return Airline
     */
    public function getAirline()
    {
        return $this->airline;
    }

    /**
     * @param Airline $airline
     */
    public function setAirline(Airline $airline): void
    {
        $this->airline = $airline;
    }

    /**
     * @return int
     */
    public function getRegionOrderPosition(): int
    {
        return $this->regionOrderPosition;
    }

    /**
     * @param int $regionOrderPosition
     */
    public function setRegionOrderPosition(int $regionOrderPosition): void
    {
        $this->regionOrderPosition = $regionOrderPosition;
    }

    /**
     * @return int
     */
    public function getAirlineOrderPosition(): int
    {
        return $this->airlineOrderPosition;
    }

    /**
     * @param int $airlineOrderPosition
     *
     * @return RegionAirline
     */
    public function setAirlineOrderPosition(int $airlineOrderPosition): RegionAirline
    {
        $this->airlineOrderPosition = $airlineOrderPosition;

        return $this;
    }
}
