<?php
/**
 * RegionAirlinesList
 *
 * Created at 2019-10-10 00:18
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 * @license GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RegionAirlinesList
 *
 * @ORM\Entity()
 * @ORM\Table(name="region_airlines_list")
 *
 * @author Serghei Niculaev <spam312sn@gmail.com>
 */
class RegionAirlinesList
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ImmutableRegion")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     *
     * @var ImmutableRegion
     */
    private $region;

    /**
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\RegionAirline",
     *     mappedBy="airlinesList",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     * @ORM\OrderBy({"airlineOrderPosition"="ASC"})
     *
     * @var array|Collection<RegionAirline>|RegionAirline[]
     */
    private $airlines;

    public function __construct(ImmutableRegion $region = null)
    {
        $this->region = $region;
        $this->airlines = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->region ? $this->region->getName() : '';
    }

    /**
     * @return ImmutableRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param ImmutableRegion $region
     */
    public function setRegion(ImmutableRegion $region): void
    {
        $this->region = $region;
    }

    /**
     * @return RegionAirline[]|array|Collection
     */
    public function getAirlines()
    {
        return $this->airlines;
    }

    /**
     * @param RegionAirline[]|array|Collection $airlines
     */
    public function setAirlines($airlines): void
    {
        $this->airlines = $airlines;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
