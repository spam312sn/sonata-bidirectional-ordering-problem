<?php

namespace AppBundle\Admin;

use AppBundle\Entity\ImmutableRegion;
use AppBundle\Entity\RegionAirlinesList;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class AirlineRegionAdmin extends AbstractAdmin
{
    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form->add(
            'airlinesList',
            ChoiceType::class,
            [
                'choices' => $this->getLists(),
            ]
        );
        $form->add('regionOrderPosition', HiddenType::class);
    }

    /**
     * @return array
     */
    private function getLists(): array
    {
        $regionsRepository = $this->getEntityManager()->getRepository(ImmutableRegion::class);
        $listRepository = $this->getEntityManager()->getRepository(RegionAirlinesList::class);

        $result = [];
        /** @var ImmutableRegion $region */
        foreach ($regionsRepository->findAll() as $region) {
            $list = $listRepository->findOneBy(['region' => $region]);
            if (null === $list) {
                $list = new RegionAirlinesList($region);
            }

            $result[$region->getName()] = $list;
        }

        return $result;
    }

    /**
     * @return EntityManager|object
     */
    private function getEntityManager(): EntityManager
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
    }
}
