<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;

class RegionAirlinesListAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('region');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Region')->add('region')->end();
        $form->with('Airlines')
            ->add(
                'airlines',
                CollectionType::class,
                [
                    'by_reference' => false,
                    'label' => false,
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'airlineOrderPosition',
                    'link_parameters' => [],
                    'admin_code' => 'app.admin.region.airline',
                ]
            )
            ->end();
    }
}
