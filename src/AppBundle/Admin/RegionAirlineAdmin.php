<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Airline;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class RegionAirlineAdmin extends AbstractAdmin
{
    /**
     * {@inheritDoc}
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form->add(
            'airline',
            EntityType::class,
            [
                'class' => Airline::class,
            ]
        );
        $form->add('airlineOrderPosition', HiddenType::class);
    }
}
