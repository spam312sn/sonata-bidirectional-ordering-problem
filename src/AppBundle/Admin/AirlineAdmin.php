<?php

declare(strict_types = 1);

namespace AppBundle\Admin;

use AppBundle\Entity\Airline;
use AppBundle\Entity\RegionAirline;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;

final class AirlineAdmin extends AbstractAdmin
{
    /**
     * @param Airline $object
     */
    public function preUpdate($object)
    {
        /** @var RegionAirline $regionAirline */
        foreach ($object->getRegions()->getIterator() as $regionAirline) {
            $regionAirline->setAirline($object);
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('name');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add(
                '_action',
                null,
                [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ],
                ]
            );
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form->with('Airline')->add('name')->end();
        $form->with('Regions')
            ->add(
                'regions',
                CollectionType::class,
                [
                    'by_reference' => false,
                    'label' => false,
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'regionOrderPosition',
                    'link_parameters' => [],
                    'admin_code' => 'app.admin.airline.regions',
                ]
            )
            ->end();
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('name');
    }
}
